package cat.dam.edgar.habitants;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintProperties;
import androidx.constraintlayout.widget.ConstraintSet;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.Normalizer;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        String[] municipis = new String[]
        {
                "Banyoles", "Camós", "Cornellà del Terri", "Crespià",
                "Esponellà", "Fontcoberta", "Palol de Revardit", "Porqueres",
                "Sant Miquel de CampMajor", "Serinyà", "Vilademuls"
        };

        String[] habitants = new String[]
        {
                "17.451", "698", "2.106", "247",
                "441", "1.212", "459", "4.208",
                "218", "1.084", "769"
        };

        super.onCreate(savedInstanceState);
        setContentView(R.layout.habitants);

        final Button btn_search = findViewById(R.id.btn_search);
        final TextView tv_num_habitants = findViewById(R.id.tv_habitants_municipi);
        final AutoCompleteTextView atv_municipis = findViewById(R.id.atv_municipi);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, municipis);
        atv_municipis.setAdapter(adapter);

        btn_search.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int index;
                String municipi;
                String error = "El municipi introduit no és del Pla de l'Estany";
                municipi = atv_municipis.getText().toString();
                index = Arrays.asList(municipis).indexOf(municipi);

                deleteImage();
                if (index == -1) {
                    tv_num_habitants.setText(error);
                } else {
                    tv_num_habitants.setText(habitants[index]);
                    createImage(municipis[index]);
                }
            }
        });
    }

    // First remove imageView from layout to prevent an already exists error
    protected void deleteImage()
    {
        ConstraintLayout layout = findViewById(R.id.main_constraint);
        ImageView deleteImage = findViewById(R.id.municipi_image);
        layout.removeView(deleteImage);
    }

    // Get municipi name replacing spaces with _ and eliminate accents (à == a) to get image name correctly
    protected String municipiName(String municipi)
    {
        String municipiCorrect;

        municipiCorrect = municipi.replaceAll(" ", "_");
        municipiCorrect = Normalizer.normalize(municipiCorrect, Normalizer.Form.NFD);
        municipiCorrect = municipiCorrect.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");

        return municipiCorrect;
    }

    protected void createImage(String municipi)
    {
        ImageView image = new ImageView(getApplicationContext());
        ConstraintLayout.LayoutParams parameters = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, 600);
        ConstraintLayout layout = findViewById(R.id.main_constraint);
        int imageResource;

        municipi = municipiName(municipi);
        imageResource = getResources().getIdentifier(municipi.toLowerCase(), "drawable", this.getPackageName());

        image.setId(R.id.municipi_image);
        image.setLayoutParams(parameters);
        image.setImageResource(imageResource);

        layout.addView(image);
        setConstraints(image, layout);
    }

    protected void setConstraints(ImageView image, ConstraintLayout layout)
    {
        ConstraintSet set = new ConstraintSet();
        TextView tv_numHabitants = findViewById(R.id.tv_habitants_municipi);
        int idLayout = layout.getId(), idImage = image.getId(), idTextView = tv_numHabitants.getId();

        set.clone(layout);
        set.connect(idImage, ConstraintSet.TOP, idTextView, ConstraintSet.BOTTOM, 64);
        set.connect(idImage, ConstraintSet.BOTTOM, idLayout, ConstraintSet.BOTTOM, 64);
        set.connect(idImage, ConstraintSet.END, idLayout, ConstraintSet.END, 0);
        set.connect(idImage, ConstraintSet.START, idLayout, ConstraintSet.START, 0);
        set.applyTo(layout);
    }


}